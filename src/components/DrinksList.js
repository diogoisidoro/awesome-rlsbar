import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import CardColumns from 'react-bootstrap/CardColumns';

const DrinksList = ({ drinks }) => {
    const [cocktails, setCocktails] = useState([]);
    useEffect(() => {
        setCocktails(drinks);
    }, [drinks]);
    return (
        <CardColumns>
            {cocktails.map((drink) => (
                <Card key={drink.id}>
                    <Card.Img variant="top" src={drink.image} />
                    <Card.Body>
                        <Card.Title>{drink.name}</Card.Title>
                        <Link to={`/drink/${drink.id}`}>
                            <Button variant="primary">More details</Button>
                        </Link>
                    </Card.Body>
                </Card>
            ))}
        </CardColumns>
    );
};

export default DrinksList;
