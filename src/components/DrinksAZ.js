import React, { useState, useEffect, useCallback } from 'react';

import { getDrinksByFirstLetter } from '../api';
import DrinksList from './DrinksList';

const CocktailList = ({ letter }) => {
    const [drinks, setDrinks] = useState([]);
    const [loading, setLoading] = useState(false);

    const fetchDrinks = useCallback(async (letter) => {
        const response = await getDrinksByFirstLetter(letter);
        setDrinks(response);
    }, []);

    useEffect(() => {
        setLoading(true);
        if (letter) fetchDrinks(letter);
        setLoading(false);
    }, [letter]);

    return (
        <div>
            <h1>Cocktails</h1>
            {letter ? <p>Letter : {letter} </p> : <p> No letter selected</p>}
            <DrinksList drinks={drinks} />
        </div>
    );
};

export default CocktailList;
