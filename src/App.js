import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Container from 'react-bootstrap/Container';

import Home from './pages/Home';
import Todo from './pages/Todo';
import Search from './pages/Search';
import Cocktails from './pages/Cocktails';
import Header from './components/Header';
import SingleCocktail from './pages/SingleCocktail';
import SingleIngredient from './pages/SingleIngredient';
import Category from './pages/Category';
import Random from './pages/Random';

function App() {
    return (
        <BrowserRouter>
            <Container className="p-3">
                <Header />
                <Switch>
                    <Route path="/random">
                        <Random />
                    </Route>
                    <Route path="/search">
                        <Search />
                    </Route>
                    <Route path="/search">
                        <Search />
                    </Route>
                    <Route path="/category/:id">
                        <Category />
                    </Route>
                    <Route path="/ingredient/:id">
                        <SingleIngredient />
                    </Route>
                    <Route path="/drink/:id">
                        <SingleCocktail />
                    </Route>
                    <Route path="/cocktails">
                        <Cocktails />
                    </Route>
                    <Route path="/todo">
                        <Todo />
                    </Route>
                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>
            </Container>
        </BrowserRouter>
    );
}

export default App;
