import React, { useState, useEffect, useCallback } from 'react';
import { Link, withRouter } from 'react-router-dom';

import Image from 'react-bootstrap/Image';
import Media from 'react-bootstrap/Media';
import ListGroup from 'react-bootstrap/ListGroup';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';

import { getDrinkById } from '../api';

const SingleCocktail = (props) => {
    const { match } = props;
    const cocktailId = match.params.id;
    const [drinkDetails, setDrinkDetails] = useState([]);
    const [ingredients, setIngredients] = useState([]);
    const [input, setInput] = useState('');

    const fetchDrink = useCallback(async (cocktailId) => {
        const response = await getDrinkById(cocktailId);
        setDrinkDetails(response);
        setIngredients(response.ingredients);
    }, []);

    useEffect(() => {
        fetchDrink(cocktailId);
    }, []);

    return (
        <Media>
            <Image
                width={200}
                height={200}
                className="mr-3"
                src={drinkDetails.image}
                roundedCircle
            />
            <Media.Body>
                <h1>
                    # {drinkDetails.id} - {drinkDetails.name}
                </h1>

                <h3>Category: {drinkDetails.category && drinkDetails.category.name}</h3>

                {ingredients ? (
                    <div>
                        <h4>Ingredients</h4>
                        <InputGroup className="mb-3">
                            <FormControl
                                placeholder="Search ingredient"
                                aria-label="Username"
                                aria-describedby="basic-addon1"
                                onChange={(ev) => setInput(ev.target.value)}
                            />
                        </InputGroup>

                        <ListGroup>
                            {ingredients.map(
                                (ingredient) =>
                                    ingredient.name.toLowerCase().startsWith(input) && (
                                        <ListGroup.Item key={ingredient.id}>
                                            <Link to={`/ingredient/${ingredient.id}`}>
                                                {ingredient.name}
                                            </Link>
                                        </ListGroup.Item>
                                    )
                            )}
                        </ListGroup>
                    </div>
                ) : (
                    <h3>Ingredients not available</h3>
                )}
            </Media.Body>
        </Media>
    );
};

export default withRouter(SingleCocktail);
