import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Nav from 'react-bootstrap/Nav';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { withRouter } from 'react-router-dom';

import { searchDrink, getDrinksByIngredient } from '../api';
import DrinkList from '../components/DrinksList';

class Search extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            drinksByName: [],
            drinksByIngredient: [],
            searchQuery: ''
        };
    }

    componentDidMount() {
        this.processSearchParam();
    }

    async fetchDrinks(searchQuery) {
        const drinksByName = await searchDrink(searchQuery);
        const drinksByIngredient = await getDrinksByIngredient(searchQuery);
        this.setState({ drinksByName, drinksByIngredient });
    }

    processSearchParam() {
        const { location } = this.props;
        const qs = new URLSearchParams(location.search);
        const searchQuery = qs.get('q');

        if (searchQuery) {
            // TODO: query api and display results
            this.fetchDrinks(searchQuery);
            console.log('searching for ' + searchQuery);
        }
    }

    render() {
        const {
            state: { drinksByName, drinksByIngredient }
        } = this;
        return (
            <Row>
                <Col sm={8}>
                    <Jumbotron>
                        <h1 className="header">RedLight Bar</h1>
                    </Jumbotron>
                </Col>
                <h2>Search results</h2>
                <h3>Results for drink name</h3>
                <DrinkList drinks={drinksByName} />
                <h3>Results for ingredients: </h3>
                <DrinkList drinks={drinksByIngredient} />
            </Row>
        );
    }
}

export default withRouter(Search);
