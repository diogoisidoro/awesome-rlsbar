import React, { useState, useEffect, useCallback } from 'react';
import { Link, withRouter } from 'react-router-dom';

import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import { getDrinksInCategory, getCategoryById } from '../api';
import DrinksList from '../components/DrinksList';

const Category = (props) => {
    const { match } = props;
    const categoryId = match.params.id;

    const [category, setCategory] = useState({});
    const [drinks, setDrinks] = useState([]);
    const [loading, setLoading] = useState(false);

    const fetchCategory = useCallback(async (categoryId) => {
        const response = await getCategoryById(categoryId);
        setCategory(response);
    }, []);

    const fetchDrinks = useCallback(async () => {
        const response = await getDrinksInCategory(categoryId);
        setDrinks(response);
    }, []);

    const sortDrinks = (order) => {
        if (order === 'ascending') {
            const drinksOrdered = [...drinks];
            setDrinks(drinksOrdered.sort((a, b) => a.name.localeCompare(b.name)));
        } else {
            const drinksOrdered = [...drinks];
            setDrinks(drinksOrdered.sort((a, b) => b.name.localeCompare(a.name)));
        }
    };

    useEffect(() => {
        setLoading(true);
        fetchCategory(categoryId);
        fetchDrinks(categoryId);
        setLoading(false);
    }, []);

    return (
        <div>
            <div>
                <Row className="justify-content-md-center">
                    <h3>{category.name}</h3>
                    <Col xs={6}>
                        <Button onClick={() => sortDrinks('ascending')}>Sort Ascending</Button>
                        <Button onClick={() => sortDrinks('descending')}>Sort Descending</Button>
                    </Col>
                </Row>
            </div>
            {drinks && <DrinksList drinks={drinks} />}
        </div>
    );
};

export default withRouter(Category);
