import React, { useState } from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import ListGroup from 'react-bootstrap/ListGroup';

import DrinksAZ from '../components/DrinksAZ';

const ALPHABET = 'abcdefghijklmnopqrstuxwxyz';

const Cocktails = () => {
    const [letter, setLetter] = useState('');
    return (
        <div>
            <Jumbotron>
                <h1 className="header">cocktails</h1>
            </Jumbotron>
            <Container>
                <Row>
                    <Col sm={3}>
                        <h2>Choose a letter</h2>
                        <ListGroup>
                            {ALPHABET.split('').map((letter) => (
                                <ListGroup.Item key={letter} onClick={() => setLetter(letter)}>
                                    {letter}
                                </ListGroup.Item>
                            ))}
                        </ListGroup>
                    </Col>
                    <Col>
                        <DrinksAZ letter={letter} />
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default Cocktails;
