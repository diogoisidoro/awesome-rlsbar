import React from 'react';
import { useHistory } from 'react-router-dom';
import { getRandomDrink } from '../api';

import Button from 'react-bootstrap/Button';

const Random = () => {
    const history = useHistory();

    const getDrink = async () => {
        const response = await getRandomDrink();
        history.push(`/drink/${response.id}`);
    };

    return <Button onClick={() => getDrink()}>Get a random drink</Button>;
};

export default Random;
