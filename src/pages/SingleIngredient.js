import React, { useState, useEffect, useCallback } from 'react';
import { withRouter } from 'react-router-dom';

import { getIngredientById } from '../api';

const SingleIngredient = (props) => {
    const { match } = props;
    const ingredientId = match.params.id;

    const [ingredient, setIngredient] = useState({});

    const fetchIngredient = useCallback(async (ingredientId) => {
        const response = await getIngredientById(ingredientId);
        setIngredient(response);
    }, []);

    useEffect(() => {
        fetchIngredient(ingredientId);
    }, []);

    return (
        <div>
            <h1>
                # {ingredient.id} - {ingredient.name}
            </h1>
        </div>
    );
};

export default withRouter(SingleIngredient);
