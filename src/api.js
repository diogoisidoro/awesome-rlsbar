const API_URL = 'http://localhost:3000';

// https://www.thecocktaildb.com/api.php

export const getRandomDrink = () => fetch(`${API_URL}/cocktails/random`).then((r) => r.json());

export const searchDrink = (term) => fetch(`${API_URL}/cocktails?q=${term}`).then((r) => r.json());

export const getCategories = () => fetch(`${API_URL}/categories`).then((r) => r.json());

export const getIngredients = () => fetch(`${API_URL}/ingredients`).then((r) => r.json());

export const getIngredientById = (id) =>
    fetch(`${API_URL}/ingredients/${id}`).then((r) => r.json());

export const getDrinkById = (id) => fetch(`${API_URL}/cocktails/${id}`).then((r) => r.json());

export const getCategoryById = (id) => fetch(`${API_URL}/categories/${id}`).then((r) => r.json());

export const getDrinksInCategory = (id) =>
    fetch(`${API_URL}/cocktails?category_id=${id}`).then((r) => r.json());

export const getDrinksByFirstLetter = (l) =>
    fetch(`${API_URL}/cocktails?starts_with=${l}`).then((r) => r.json());

export const getDrinksByIngredient = (ingredient) =>
    fetch(`${API_URL}/cocktails?ingredient=${ingredient}`).then((r) => r.json());
